# OpenML dataset: Land-Market-in-Saudi-Arabia

https://www.openml.org/d/43498

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset has been scrapped off sa.aqar.fm to obtain land information such as price, size, street width, and locations. The uncleaned dataset scrapped 4347 rows, but seems like 1395 were duplicated and deleted. Leaving us with 2952 rows. The majority of the data is unfortunately lands based in Riyadh, but there are a good number of lands in Jeddah and Khobar. The dataset is set in Arabia, and decided to keep it that way to keep the integrity of the data. However, the street width, land size, and price have all been converted to numerical values, leaving some of those values as null if couldn't convert them.
Here is the data description:
mainlocation        object        The main location of the land
sublocation        object        Indicates the subregion of the location. Note that only the big cities (ex. Riyadh and Jeddah) have subregions, NaN values are meant to be empty
neighborhood        object        The neighborhood where the land resides
frontage        object        The cardinal direction where the land faces the street
purpose        object        The purpose for land use
streetwidth        int        The length of the street facing the land in meters
size        int        The size of the land in meters squared
Pricepm        int        The price per meters squared

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43498) of an [OpenML dataset](https://www.openml.org/d/43498). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43498/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43498/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43498/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

